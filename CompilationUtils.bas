Attribute VB_Name = "CompilationUtils"
'Pour tableau ETAT_ACTUEL
Public Function convertirLigneEnDonneesFicheA(ligne As Integer, tbl As ListObject) As DonnesFicheComplet
    
    Dim donnesFiche As DonnesFicheComplet
    Set donnesFiche = New DonnesFicheComplet
    
    donnesFiche.spe = tbl.DataBodyRange(ligne, 1).Value
    donnesFiche.emetteur = tbl.DataBodyRange(ligne, 2).Value
    donnesFiche.typeDoc = tbl.DataBodyRange(ligne, 3).Value
    donnesFiche.avisComp = tbl.DataBodyRange(ligne, 4).Value
    donnesFiche.etatDoc = tbl.DataBodyRange(ligne, 5).Value
    donnesFiche.statutBordereau = tbl.DataBodyRange(ligne, 6).Value
    donnesFiche.DateEnvoiValid = tbl.DataBodyRange(ligne, 7).Value
    donnesFiche.numGED = tbl.DataBodyRange(ligne, 8).Value
    donnesFiche.indiceGED = tbl.DataBodyRange(ligne, 9).Value
    donnesFiche.codeGED = tbl.DataBodyRange(ligne, 10).Value
    donnesFiche.TITRE = tbl.DataBodyRange(ligne, 11).Value
    donnesFiche.objEnvoi = tbl.DataBodyRange(ligne, 12).Value
    
    donnesFiche.uRecev = tbl.DataBodyRange(ligne, 14).Value
    donnesFiche.recev = tbl.DataBodyRange(ligne, 15).Value
    donnesFiche.comRecev = tbl.DataBodyRange(ligne, 16).Value
    donnesFiche.DateRecev = tbl.DataBodyRange(ligne, 17).Value
    donnesFiche.ficheRecev = tbl.DataBodyRange(ligne, 18).Value
    
    donnesFiche.u1 = tbl.DataBodyRange(ligne, 20).Value
    donnesFiche.recev1 = tbl.DataBodyRange(ligne, 21).Value
    donnesFiche.com1 = tbl.DataBodyRange(ligne, 22).Value
    donnesFiche.Date1 = tbl.DataBodyRange(ligne, 23).Value
    donnesFiche.fiche1 = tbl.DataBodyRange(ligne, 24).Value
    
    donnesFiche.u2 = tbl.DataBodyRange(ligne, 26).Value
    donnesFiche.recev2 = tbl.DataBodyRange(ligne, 27).Value
    donnesFiche.com2 = tbl.DataBodyRange(ligne, 28).Value
    donnesFiche.Date2 = tbl.DataBodyRange(ligne, 29).Value
    donnesFiche.fiche2 = tbl.DataBodyRange(ligne, 30).Value
    
    donnesFiche.u3 = tbl.DataBodyRange(ligne, 32).Value
    donnesFiche.recev3 = tbl.DataBodyRange(ligne, 33).Value
    donnesFiche.com3 = tbl.DataBodyRange(ligne, 34).Value
    donnesFiche.Date3 = tbl.DataBodyRange(ligne, 35).Value
    donnesFiche.fiche3 = tbl.DataBodyRange(ligne, 36).Value
    
    donnesFiche.u4 = tbl.DataBodyRange(ligne, 38).Value
    donnesFiche.recev4 = tbl.DataBodyRange(ligne, 39).Value
    donnesFiche.com4 = tbl.DataBodyRange(ligne, 40).Value
    donnesFiche.Date4 = tbl.DataBodyRange(ligne, 41).Value
    donnesFiche.fiche4 = tbl.DataBodyRange(ligne, 42).Value
    
    donnesFiche.u5 = tbl.DataBodyRange(ligne, 44).Value
    donnesFiche.recev5 = tbl.DataBodyRange(ligne, 45).Value
    donnesFiche.com5 = tbl.DataBodyRange(ligne, 46).Value
    donnesFiche.Date5 = tbl.DataBodyRange(ligne, 47).Value
    donnesFiche.fiche5 = tbl.DataBodyRange(ligne, 48).Value
    
    donnesFiche.uVF = tbl.DataBodyRange(ligne, 50).Value
    donnesFiche.recevVF = tbl.DataBodyRange(ligne, 51).Value
    donnesFiche.comVF = tbl.DataBodyRange(ligne, 52).Value
    donnesFiche.DateVF = tbl.DataBodyRange(ligne, 53).Value
    donnesFiche.ficheVF = tbl.DataBodyRange(ligne, 54).Value
    
    Debug.Print "---------------"
    'Debug.Print Format(CDate(tbl.DataBodyRange(ligne, 55).Value), "mm/dd/yyyy")
    
    If Not tbl.DataBodyRange(ligne, 55) Is Nothing Then
        donnesFiche.DatePrev = tbl.DataBodyRange(ligne, 55).Value
    End If
    'donnesFiche.DatePrev = Format(tbl.DataBodyRange(ligne, 55).Value, "dd/mm/yyyy")
    
    Set convertirLigneEnDonneesFicheA = donnesFiche


End Function

'Pour tableau RAPPORT_AVANCEMENT_XX
Public Function convertirLigneEnDonneesFicheB(ligne As Integer, tbl As ListObject) As DonnesFicheComplet

    Dim donnesFiche As DonnesFicheComplet
    Set donnesFiche = New DonnesFicheComplet
    
    donnesFiche.spe = tbl.DataBodyRange(ligne, 7).Value
    donnesFiche.emetteur = tbl.DataBodyRange(ligne, 8).Value
    donnesFiche.typeDoc = tbl.DataBodyRange(ligne, 12).Value
    donnesFiche.avisComp = tbl.DataBodyRange(ligne, 13).Value
    donnesFiche.etatDoc = tbl.DataBodyRange(ligne, 14).Value
    donnesFiche.statutBordereau = tbl.DataBodyRange(ligne, 25).Value
    donnesFiche.DateEnvoiValid = tbl.DataBodyRange(ligne, 24).Value
    donnesFiche.numGED = tbl.DataBodyRange(ligne, 16).Value
    donnesFiche.indiceGED = tbl.DataBodyRange(ligne, 17).Value
    donnesFiche.codeGED = tbl.DataBodyRange(ligne, 18).Value
    donnesFiche.TITRE = tbl.DataBodyRange(ligne, 19).Value
    donnesFiche.objEnvoi = tbl.DataBodyRange(ligne, 20).Value
    
    donnesFiche.uRecev = tbl.DataBodyRange(ligne, 30).Value
    donnesFiche.recev = tbl.DataBodyRange(ligne, 31).Value
    donnesFiche.comRecev = tbl.DataBodyRange(ligne, 32).Value
    donnesFiche.DateRecev = tbl.DataBodyRange(ligne, 34).Value
    donnesFiche.ficheRecev = tbl.DataBodyRange(ligne, 35).Value
    
    donnesFiche.u1 = tbl.DataBodyRange(ligne, 39).Value
    donnesFiche.recev1 = tbl.DataBodyRange(ligne, 40).Value
    donnesFiche.com1 = tbl.DataBodyRange(ligne, 41).Value
    donnesFiche.Date1 = tbl.DataBodyRange(ligne, 43).Value
    donnesFiche.fiche1 = tbl.DataBodyRange(ligne, 44).Value
    
    donnesFiche.u2 = tbl.DataBodyRange(ligne, 48).Value
    donnesFiche.recev2 = tbl.DataBodyRange(ligne, 49).Value
    donnesFiche.com2 = tbl.DataBodyRange(ligne, 50).Value
    donnesFiche.Date2 = tbl.DataBodyRange(ligne, 52).Value
    donnesFiche.fiche2 = tbl.DataBodyRange(ligne, 53).Value
    
    donnesFiche.u3 = tbl.DataBodyRange(ligne, 57).Value
    donnesFiche.recev3 = tbl.DataBodyRange(ligne, 58).Value
    donnesFiche.com3 = tbl.DataBodyRange(ligne, 59).Value
    donnesFiche.Date3 = tbl.DataBodyRange(ligne, 61).Value
    donnesFiche.fiche3 = tbl.DataBodyRange(ligne, 62).Value
    
    donnesFiche.u4 = tbl.DataBodyRange(ligne, 66).Value
    donnesFiche.recev4 = tbl.DataBodyRange(ligne, 67).Value
    donnesFiche.com4 = tbl.DataBodyRange(ligne, 68).Value
    donnesFiche.Date4 = tbl.DataBodyRange(ligne, 70).Value
    donnesFiche.fiche4 = tbl.DataBodyRange(ligne, 71).Value
    
    donnesFiche.u5 = tbl.DataBodyRange(ligne, 75).Value
    donnesFiche.recev5 = tbl.DataBodyRange(ligne, 76).Value
    donnesFiche.com5 = tbl.DataBodyRange(ligne, 77).Value
    donnesFiche.Date5 = tbl.DataBodyRange(ligne, 79).Value
    donnesFiche.fiche5 = tbl.DataBodyRange(ligne, 80).Value
    
    donnesFiche.uVF = tbl.DataBodyRange(ligne, 120).Value
    donnesFiche.recevVF = tbl.DataBodyRange(ligne, 121).Value
    donnesFiche.comVF = tbl.DataBodyRange(ligne, 122).Value
    donnesFiche.DateVF = tbl.DataBodyRange(ligne, 124).Value
    donnesFiche.ficheVF = tbl.DataBodyRange(ligne, 125).Value
    donnesFiche.DatePrev = tbl.DataBodyRange(ligne, 126).Value
    
    Set convertirLigneEnDonneesFicheB = donnesFiche
    
End Function

