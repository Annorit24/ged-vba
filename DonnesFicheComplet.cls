VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DonnesFicheComplet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private cspe As String
Private cemetteur As String
Private cTypeDoc As String
Private cavisComp As String
Private cetatDoc As String
Private cstatutBordereau As String
Private cDateEnvoiValid As String
Private cnumGED As String
Private cindiceGED As String
Private ccodeGED As String
Private ctitre As String
Private cObjEnvoi As String

Private cuRecev As String
Private crecev As String
Private ccomRecev As String
Private cDateRecev As String
Private cficheRecev As String

Private cu1 As String
Private crecev1 As String
Private ccom1 As String
Private cDate1 As String
Private cfiche1 As String

Private cu2 As String
Private crecev2 As String
Private ccom2 As String
Private cDate2 As String
Private cfiche2 As String

Private cu3 As String
Private crecev3 As String
Private ccom3 As String
Private cDate3 As String
Private cfiche3 As String

Private cu4 As String
Private crecev4 As String
Private ccom4 As String
Private cDate4 As String
Private cfiche4 As String

Private cu5 As String
Private crecev5 As String
Private ccom5 As String
Private cDate5 As String
Private cfiche5 As String

Private cuVF As String
Private crecevVF As String
Private ccomVF As String
Private cDateVF As String
Private cficheVF As String
Private cDatePrev As String

Public Sub Class_Initialize()

End Sub

Public Property Get getSpe() As String
    getSpe = cspe
End Property

Public Property Let spe(Value As String)
    cspe = Value
End Property

Public Property Get getEmetteur() As String
    getEmetteur = cemetteur
End Property

Public Property Let emetteur(Value As String)
    cemetteur = Value
End Property

Public Property Get getTypeDoc() As String
    getTypeDoc = cTypeDoc
End Property

Public Property Let typeDoc(Value As String)
    cTypeDoc = Value
End Property

Public Property Get getAvisComp() As String
    getAvisComp = cavisComp
End Property

Public Property Let avisComp(Value As String)
    cavisComp = Value
End Property

Public Property Get getEtatDoc() As String
    getEtatDoc = cetatDoc
End Property

Public Property Let etatDoc(Value As String)
    cetatDoc = Value
End Property

Public Property Get getStatutBordereau() As String
    getStatutBordereau = cstatutBordereau
End Property

Public Property Let statutBordereau(Value As String)
    cstatutBordereau = Value
End Property

Public Property Get getDateEnvoiValid() As String
    getDateEnvoiValid = cDateEnvoiValid
End Property

Public Property Let DateEnvoiValid(Value As String)
    cDateEnvoiValid = Value
End Property

Public Property Get getNumGED() As String
    getNumGED = cnumGED
End Property

Public Property Let numGED(Value As String)
    cnumGED = Value
End Property

Public Property Get getIndiceGED() As String
    getIndiceGED = cindiceGED
End Property

Public Property Let indiceGED(Value As String)
    cindiceGED = Value
End Property

Public Property Get getCodeGED() As String
    getCodeGED = ccodeGED
End Property

Public Property Let codeGED(Value As String)
    ccodeGED = Value
End Property

Public Property Get getTitre() As String
    getTitre = ctitre
End Property

Public Property Let TITRE(Value As String)
    ctitre = Value
End Property

Public Property Get getObjEnvoi() As String
    getObjEnvoi = cObjEnvoi
End Property

Public Property Let objEnvoi(Value As String)
    cObjEnvoi = Value
End Property

''''''''''''''''''''''''''''''''''''''''''''''''

Public Property Get getURecev() As String
    getURecev = cuRecev
End Property

Public Property Let uRecev(Value As String)
    cuRecev = Value
End Property

Public Property Get getRecev() As String
    getRecev = crecev
End Property

Public Property Let recev(Value As String)
    crecev = Value
End Property

Public Property Get getComRecev() As String
    getComRecev = ccomRecev
End Property

Public Property Let comRecev(Value As String)
    ccomRecev = Value
End Property

Public Property Get getDateRecev() As String
    getDateRecev = cDateRecev
End Property

Public Property Let DateRecev(Value As String)
    cDateRecev = Value
End Property

Public Property Get getFicheRecev() As String
    getFicheRecev = cficheRecev
End Property

Public Property Let ficheRecev(Value As String)
    cficheRecev = Value
End Property

''''''''''''''''''''''''''''''''''''''''''''''''

Public Property Get getU1() As String
    getU1 = cu1
End Property

Public Property Let u1(Value As String)
    cu1 = Value
End Property

Public Property Get getRecev1() As String
    getRecev1 = crecev1
End Property

Public Property Let recev1(Value As String)
    crecev1 = Value
End Property

Public Property Get getCom1() As String
    getCom1 = ccom1
End Property

Public Property Let com1(Value As String)
    ccom1 = Value
End Property

Public Property Get getDate1() As String
    getDate1 = cDate1
End Property

Public Property Let Date1(Value As String)
    cDate1 = Value
End Property

Public Property Get getFiche1() As String
    getFiche1 = cfiche1
End Property

Public Property Let fiche1(Value As String)
    cfiche1 = Value
End Property

''''''''''''''''''''''''''''''''''''''''''''''''

Public Property Get getU2() As String
    getU2 = cu2
End Property

Public Property Let u2(Value As String)
    cu2 = Value
End Property

Public Property Get getRecev2() As String
    getRecev2 = crecev2
End Property

Public Property Let recev2(Value As String)
    crecev2 = Value
End Property

Public Property Get getCom2() As String
    getCom2 = ccom2
End Property

Public Property Let com2(Value As String)
    ccom2 = Value
End Property

Public Property Get getDate2() As String
    getDate2 = cDate2
End Property

Public Property Let Date2(Value As String)
    cDate2 = Value
End Property

Public Property Get getFiche2() As String
    getFiche2 = cfiche2
End Property

Public Property Let fiche2(Value As String)
    cfiche2 = Value
End Property

''''''''''''''''''''''''''''''''''''''''''''''''

Public Property Get getU3() As String
    getU3 = cu3
End Property

Public Property Let u3(Value As String)
    cu3 = Value
End Property

Public Property Get getRecev3() As String
    getRecev3 = crecev3
End Property

Public Property Let recev3(Value As String)
    crecev3 = Value
End Property

Public Property Get getCom3() As String
    getCom3 = ccom3
End Property

Public Property Let com3(Value As String)
    ccom3 = Value
End Property

Public Property Get getDate3() As String
    getDate3 = cDate3
End Property

Public Property Let Date3(Value As String)
    cDate3 = Value
End Property

Public Property Get getFiche3() As String
    getFiche3 = cfiche3
End Property

Public Property Let fiche3(Value As String)
    cfiche3 = Value
End Property

''''''''''''''''''''''''''''''''''''''''''''''''

Public Property Get getU4() As String
    getU4 = cu4
End Property

Public Property Let u4(Value As String)
    cu4 = Value
End Property

Public Property Get getRecev4() As String
    getRecev4 = crecev4
End Property

Public Property Let recev4(Value As String)
    crecev4 = Value
End Property

Public Property Get getCom4() As String
    getCom4 = ccom4
End Property

Public Property Let com4(Value As String)
    ccom4 = Value
End Property

Public Property Get getDate4() As String
    getDate4 = cDate4
End Property

Public Property Let Date4(Value As String)
    cDate4 = Value
End Property

Public Property Get getFiche4() As String
    getFiche4 = cfiche4
End Property

Public Property Let fiche4(Value As String)
    cfiche4 = Value
End Property

''''''''''''''''''''''''''''''''''''''''''''''''

Public Property Get getU5() As String
    getU5 = cu5
End Property

Public Property Let u5(Value As String)
    cu5 = Value
End Property

Public Property Get getRecev5() As String
    getRecev5 = crecev5
End Property

Public Property Let recev5(Value As String)
    crecev5 = Value
End Property

Public Property Get getCom5() As String
    getCom5 = ccom5
End Property

Public Property Let com5(Value As String)
    ccom5 = Value
End Property

Public Property Get getDate5() As String
    getDate5 = cDate5
End Property

Public Property Let Date5(Value As String)
    cDate5 = Value
End Property

Public Property Get getFiche5() As String
    getFiche5 = cfiche5
End Property

Public Property Let fiche5(Value As String)
    cfiche5 = Value
End Property

''''''''''''''''''''''''''''''''''''''''''''''''

Public Property Get getUVF() As String
    getUVF = cuVF
End Property

Public Property Let uVF(Value As String)
    cuVF = Value
End Property

Public Property Get getRecevVF() As String
    getRecevVF = crecevVF
End Property

Public Property Let recevVF(Value As String)
    crecevVF = Value
End Property

Public Property Get getComVF() As String
    getComVF = ccomVF
End Property

Public Property Let comVF(Value As String)
    ccomVF = Value
End Property

Public Property Get getDateVF() As String
    getDateVF = cDateVF
End Property

Public Property Let DateVF(Value As String)
    cDateVF = Value
End Property

Public Property Get getFicheVF() As String
    getFicheVF = cficheVF
End Property

Public Property Let ficheVF(Value As String)
    cficheVF = Value
End Property

Public Property Get getDatePrev() As String
    getDatePrev = cDatePrev
End Property

Public Property Let DatePrev(Value As String)
    cDatePrev = Value
End Property

'''''''''''''''''''''''''''''''''''''''

Public Function egale(other As DonnesFicheComplet) As Boolean

    egale = cnumGED = Replace(other.getNumGED, "0", "") And _
    Len(ctitre) = Len(other.getTitre) And _
    cetatDoc = other.getEtatDoc And _
    cstatutBordereau = other.getStatutBordereau And _
    cindiceGED = other.getIndiceGED And _
    crecev = other.getRecev And _
    crecev1 = other.getRecev1 And _
    crecev2 = other.getRecev2 And _
    crecev3 = other.getRecev3 And _
    crecev4 = other.getRecev4 And _
    crecev5 = other.getRecev5 And _
    crecevVF = other.getRecevVF

End Function

Public Function applyToCompil(ligne As String, tbl As ListObject)

    tbl.DataBodyRange(ligne, 1).Value = cspe
    tbl.DataBodyRange(ligne, 2).Value = cemetteur
    tbl.DataBodyRange(ligne, 3).Value = cTypeDoc
    tbl.DataBodyRange(ligne, 4).Value = cavisComp
    tbl.DataBodyRange(ligne, 5).Value = cetatDoc
    tbl.DataBodyRange(ligne, 6).Value = cstatutBordereau
    tbl.DataBodyRange(ligne, 7).Value = Format(cDateEnvoiValid, "mm/dd/yyyy")
    tbl.DataBodyRange(ligne, 8).Value = cnumGED
    tbl.DataBodyRange(ligne, 9).Value = cindiceGED
    tbl.DataBodyRange(ligne, 10).Value = ccodeGED
    tbl.DataBodyRange(ligne, 11).Value = ctitre
    tbl.DataBodyRange(ligne, 12).Value = cObjEnvoi
    
    tbl.DataBodyRange(ligne, 14).Value = cuRecev
    tbl.DataBodyRange(ligne, 15).Value = crecev
    tbl.DataBodyRange(ligne, 16).Value = ccomRecev
    tbl.DataBodyRange(ligne, 17).Value = Format(cDateRecev, "mm/dd/yyyy")
    tbl.DataBodyRange(ligne, 18).Value = cficheRecev
    
    tbl.DataBodyRange(ligne, 20).Value = cu1
    tbl.DataBodyRange(ligne, 21).Value = crecev1
    tbl.DataBodyRange(ligne, 22).Value = ccom1
    tbl.DataBodyRange(ligne, 23).Value = Format(cDate1, "mm/dd/yyyy")
    tbl.DataBodyRange(ligne, 24).Value = cfiche1
    
    tbl.DataBodyRange(ligne, 26).Value = cu2
    tbl.DataBodyRange(ligne, 27).Value = crecev2
    tbl.DataBodyRange(ligne, 28).Value = ccom2
    tbl.DataBodyRange(ligne, 29).Value = Format(cDate2, "mm/dd/yyyy")
    tbl.DataBodyRange(ligne, 30).Value = cfiche2
    
    tbl.DataBodyRange(ligne, 32).Value = cu3
    tbl.DataBodyRange(ligne, 33).Value = crecev3
    tbl.DataBodyRange(ligne, 34).Value = ccom3
    tbl.DataBodyRange(ligne, 35).Value = Format(cDate3, "mm/dd/yyyy")
    tbl.DataBodyRange(ligne, 36).Value = cfiche3
    
    tbl.DataBodyRange(ligne, 38).Value = cu4
    tbl.DataBodyRange(ligne, 39).Value = crecev4
    tbl.DataBodyRange(ligne, 40).Value = ccom4
    tbl.DataBodyRange(ligne, 41).Value = Format(cDate4, "mm/dd/yyyy")
    tbl.DataBodyRange(ligne, 42).Value = cfiche4
    
    tbl.DataBodyRange(ligne, 44).Value = cu5
    tbl.DataBodyRange(ligne, 45).Value = crecev5
    tbl.DataBodyRange(ligne, 46).Value = ccom5
    tbl.DataBodyRange(ligne, 47).Value = Format(cDate5, "mm/dd/yyyy")
    tbl.DataBodyRange(ligne, 48).Value = cfiche5
    
    tbl.DataBodyRange(ligne, 50).Value = cuVF
    tbl.DataBodyRange(ligne, 51).Value = crecevVF
    tbl.DataBodyRange(ligne, 52).Value = ccomVF
    tbl.DataBodyRange(ligne, 53).Value = Format(cDateVF, "mm/dd/yyyy")
    tbl.DataBodyRange(ligne, 54).Value = cficheVF
    tbl.DataBodyRange(ligne, 55).Value = Format(cDatePrev, "mm/dd/yyyy")
    
    

End Function

Public Function insertToCompil(tbl As ListObject)

    Dim tata As Date

    Dim ligne As String

    tbl.ListRows.Add AlwaysInsert:=True
    
    ligne = tbl.ListRows.count

    tbl.DataBodyRange(ligne, 1).Value = cspe
    tbl.DataBodyRange(ligne, 2).Value = cemetteur
    tbl.DataBodyRange(ligne, 3).Value = cTypeDoc
    tbl.DataBodyRange(ligne, 4).Value = cavisComp
    tbl.DataBodyRange(ligne, 5).Value = cetatDoc
    tbl.DataBodyRange(ligne, 6).Value = cstatutBordereau
    tbl.DataBodyRange(ligne, 7).Value = Format(cDateEnvoiValid, "mm/dd/yyyy")
    tbl.DataBodyRange(ligne, 8).Value = cnumGED
    tbl.DataBodyRange(ligne, 9).Value = cindiceGED
    tbl.DataBodyRange(ligne, 10).Value = ccodeGED
    tbl.DataBodyRange(ligne, 11).Value = ctitre
    tbl.DataBodyRange(ligne, 12).Value = cObjEnvoi
    
    tbl.DataBodyRange(ligne, 14).Value = cuRecev
    tbl.DataBodyRange(ligne, 15).Value = crecev
    tbl.DataBodyRange(ligne, 16).Value = ccomRecev
    tbl.DataBodyRange(ligne, 17).Value = Format(cDateRecev, "dd/mm/yyyy")
    tbl.DataBodyRange(ligne, 18).Value = cficheRecev
    
    tbl.DataBodyRange(ligne, 20).Value = cu1
    tbl.DataBodyRange(ligne, 21).Value = crecev1
    tbl.DataBodyRange(ligne, 22).Value = ccom1
    tbl.DataBodyRange(ligne, 23).Value = Format(cDate1, "dd/mm/yyyy")
    tbl.DataBodyRange(ligne, 24).Value = cfiche1
    
    tbl.DataBodyRange(ligne, 26).Value = cu2
    tbl.DataBodyRange(ligne, 27).Value = crecev2
    tbl.DataBodyRange(ligne, 28).Value = ccom2
    tbl.DataBodyRange(ligne, 29).Value = Format(cDate2, "dd/mm/yyyy")
    tbl.DataBodyRange(ligne, 30).Value = cfiche2
    
    tbl.DataBodyRange(ligne, 32).Value = cu3
    tbl.DataBodyRange(ligne, 33).Value = crecev3
    tbl.DataBodyRange(ligne, 34).Value = ccom3
    tbl.DataBodyRange(ligne, 35).Value = Format(cDate3, "dd/mm/yyyy")
    tbl.DataBodyRange(ligne, 36).Value = cfiche3
    
    tbl.DataBodyRange(ligne, 38).Value = cu4
    tbl.DataBodyRange(ligne, 39).Value = crecev4
    tbl.DataBodyRange(ligne, 40).Value = ccom4
    tbl.DataBodyRange(ligne, 41).Value = Format(cDate4, "dd/mm/yyyy")
    tbl.DataBodyRange(ligne, 42).Value = cfiche4
    
    tbl.DataBodyRange(ligne, 44).Value = cu5
    tbl.DataBodyRange(ligne, 45).Value = crecev5
    tbl.DataBodyRange(ligne, 46).Value = ccom5
    tbl.DataBodyRange(ligne, 47).Value = Format(cDate5, "dd/mm/yyyy")
    tbl.DataBodyRange(ligne, 48).Value = cfiche5
    
    tbl.DataBodyRange(ligne, 50).Value = cuVF
    tbl.DataBodyRange(ligne, 51).Value = crecevVF
    tbl.DataBodyRange(ligne, 52).Value = ccomVF
    tbl.DataBodyRange(ligne, 53).Value = Format(cDateVF, "dd/mm/yyyy")
    tbl.DataBodyRange(ligne, 54).Value = cficheVF
    tbl.DataBodyRange(ligne, 55).Value = cDatePrev

End Function
