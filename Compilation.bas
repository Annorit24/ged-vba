Attribute VB_Name = "Compilation"
Public Sub compilation()

    Dim tableauPG As ListObject
    Set tableauPG = ThisWorkbook.Sheets(Utils.PG_SHEET_INDEX).ListObjects(1)
    
    Dim tableauPE As ListObject
    Set tableauPE = ThisWorkbook.Sheets(Utils.PE_SHEET_INDEX).ListObjects(1)

    Dim tableauCOMP As ListObject
    Set tableauCOMP = ThisWorkbook.Sheets(Utils.COMP_SHEET_INDEX).ListObjects(1)
    
    Call recupererDonnees(tableauCOMP, tableauPE)
    Call recupererDonnees(tableauCOMP, tableauPG)

    

End Sub


Private Function recupererDonnees(tableauCOMP As ListObject, tableauPX As ListObject)

    Dim i As Integer
    For i = 1 To tableauPX.Range.Rows.count
    
        Debug.Print i
    
        Dim codeGED As String
        codeGED = tableauPX.DataBodyRange(i, Utils.CODE_GED_INDEX).Value
        
        Dim foundCell As Range
        Set foundCell = tableauCOMP.DataBodyRange.Columns(10).Find(codeGED)
        
        'Le document existe
        If Not (foundCell Is Nothing) Then
            Dim val As String
            val = foundCell.Value
            
            If Not val = "" Then
                Dim donneesFicheActuel As DonnesFicheComplet
                Dim donneesFicheNouveau As DonnesFicheComplet
                
                Set donneesFicheActuel = CompilationUtils.convertirLigneEnDonneesFicheA(foundCell.Row - tableauCOMP.HeaderRowRange.Row, tableauCOMP)
                Set donneesFicheNouveau = CompilationUtils.convertirLigneEnDonneesFicheB(i, tableauPX)
                
                Debug.Print codeGED
                
                If Not donneesFicheActuel.egale(donneesFicheNouveau) Then
                    Debug.Print "here"
                    
                    Call donneesFicheNouveau.applyToCompil(foundCell.Row - tableauCOMP.HeaderRowRange.Row, tableauCOMP)
                    tableauCOMP.DataBodyRange(foundCell.Row - tableauCOMP.HeaderRowRange.Row, 61).Value = True
                    tableauCOMP.DataBodyRange(foundCell.Row - tableauCOMP.HeaderRowRange.Row, 62).Value = False
                Else
                    Debug.Print "hello"
                    tableauCOMP.DataBodyRange(foundCell.Row - tableauCOMP.HeaderRowRange.Row, 61).Value = False
                    tableauCOMP.DataBodyRange(foundCell.Row - tableauCOMP.HeaderRowRange.Row, 62).Value = False
                End If
                
                
            End If
            
            
        'Le document n'existe pas
        Else
        
            Dim donnesFicheNouveau1 As DonnesFicheComplet
            Set donnesFicheNouveau1 = CompilationUtils.convertirLigneEnDonneesFicheB(i, tableauPX)
            
            Debug.Print "Not found"
            'Debug.Print donnesFicheNouveau1.toString
            donnesFicheNouveau1.insertToCompil tableauCOMP
            ligne = tableauCOMP.ListRows.count
            tableauCOMP.DataBodyRange(ligne, 61).Value = False
            tableauCOMP.DataBodyRange(ligne, 62).Value = True
        
        End If
        
    
    
    Next

End Function
