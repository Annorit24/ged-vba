Attribute VB_Name = "CodeGedFiltre"
Sub run()
Attribute run.VB_ProcData.VB_Invoke_Func = " \n14"
    Dim num As String
    num = Cells(2, 6).Value
    
    ActiveSheet.ListObjects("Suivis_Synthétique").Range.AutoFilter Field:=6, Criteria1:=num
End Sub


Sub del()
Attribute del.VB_ProcData.VB_Invoke_Func = " \n14"
    ActiveSheet.ListObjects("Suivis_Synthétique").Range.AutoFilter Field:=6
End Sub

Sub clean()
Attribute clean.VB_ProcData.VB_Invoke_Func = " \n14"

    'Rows("14:14").RowHeight = 55.5
    Columns("A:A").ColumnWidth = 130
    Columns("B:B").ColumnWidth = 9
    Columns("C:C").ColumnWidth = 10.5
    Columns("D:D").ColumnWidth = 9.1
    Columns("E:E").ColumnWidth = 18.5
    Columns("F:F").ColumnWidth = 10
    Columns("G:G").ColumnWidth = 5
    Columns("H:H").ColumnWidth = 12
    Columns("I:I").ColumnWidth = 12
    Columns("J:J").ColumnWidth = 10.5
    Columns("K:K").ColumnWidth = 12
    Columns("L:L").ColumnWidth = 10.5
    Columns("M:M").ColumnWidth = 13
    Columns("N:N").ColumnWidth = 19
    Columns("O:O").ColumnWidth = 19
    Columns("P:P").ColumnWidth = 10.5
    Columns("Q:Q").ColumnWidth = 25
    
End Sub
