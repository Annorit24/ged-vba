VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DonnesFiche"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private ccodeGED As String
Private ctitre As String
Private cetatDoc As String
Private cstatutBordereau As String
Private cpremierRegard As String
Private cparr1 As String
Private cparr2 As String
Private cparr3 As String
Private cparr4 As String
Private cparr5 As String
Private cvfrecevabilite As String
Private cmodif As String
Private cnouveau As String

Public Sub Class_Initialize()

End Sub

Public Property Get getCodeGED() As String
    getCodeGED = ccodeGED
End Property

Public Property Let codeGED(Value As String)
    ccodeGED = Value
End Property

Public Property Get getTitre() As String
    getTitre = ctitre
End Property

Public Property Let TITRE(Value As String)
    ctitre = Value
End Property

Public Property Get getEtatDoc() As String
    getEtatDoc = cetatDoc
End Property

Public Property Let etatDoc(Value As String)
    cetatDoc = Value
End Property

Public Property Get getStatutBordereau() As String
    getStatutBordereau = cstatutBordereau
End Property

Public Property Let statutBordereau(Value As String)
    cstatutBordereau = Value
End Property

Public Property Get getPremierRegard() As String
    getPremierRegard = cpremierRegard
End Property

Public Property Let premierRegard(Value As String)
    cpremierRegard = Value
End Property

Public Property Get getParr1() As String
    getParr1 = cparr1
End Property

Public Property Let parr1(Value As String)
    cparr1 = Value
End Property

Public Property Get getParr2() As String
    getParr2 = cparr2
End Property

Public Property Let parr2(Value As String)
    cparr2 = Value
End Property

Public Property Get getParr3() As String
    getParr3 = cparr3
End Property

Public Property Let parr3(Value As String)
    cparr3 = Value
End Property

Public Property Get getParr4() As String
    getParr4 = cparr4
End Property

Public Property Let parr4(Value As String)
    cparr4 = Value
End Property

Public Property Get getParr5() As String
    getParr5 = cparr5
End Property

Public Property Let parr5(Value As String)
    cparr5 = Value
End Property

Public Property Get getVfrecevabilite() As String
    getVfrecevabilite = cvfrecevabilite
End Property

Public Property Let vfrecevabilite(Value As String)
    cvfrecevabilite = Value
End Property

Public Property Get getModif() As String
    getModif = cmodif
End Property

Public Property Let modif(Value As String)
    cmodif = Value
End Property

Public Property Get getNouveau() As String
    getNouveau = cnouveau
End Property

Public Property Let nouveau(Value As String)
    cnouveau = Value
End Property

Public Function equals(other As donnesFiche) As Boolean

    equals = ccodeGED = other.getCodeGED And _
    ctitre = other.getTitre And _
    cetatDoc = other.getEtatDoc And _
    cstatutBordereau = other.getStatutBordereau And _
    cpremierRegard = other.getPremierRegard And _
    cparr1 = other.getParr1 And _
    cparr2 = other.getParr2 And _
    cparr3 = other.getParr3 And _
    cparr4 = other.getParr4 And _
    cparr5 = other.getParr5 And _
    cvfrecevabilite = other.getVfrecevabilite
    
End Function

Public Function toString() As String
    toString = "[" & ccodeGED & "; " & ctitre & "; " & cstatutBordereau & "; " & cpremierRegard & "; " & cparr1 & "; " & cparr2 & "; " & cparr3 & "; " & cparr4 & "; " & cparr5 & "; " & cvfrecevabilite & "]"
End Function

Public Function appyToEtatActuel(ligne As Integer, tbl As ListObject)

    tbl.DataBodyRange(ligne, 1).Value = ccodeGED
    tbl.DataBodyRange(ligne, 2).Value = ctitre
    tbl.DataBodyRange(ligne, 3).Value = cetatDoc
    tbl.DataBodyRange(ligne, 4).Value = cstatutBordereau
    tbl.DataBodyRange(ligne, 5).Value = cpremierRegard
    tbl.DataBodyRange(ligne, 6).Value = cparr1
    tbl.DataBodyRange(ligne, 7).Value = cparr2
    tbl.DataBodyRange(ligne, 8).Value = cparr3
    tbl.DataBodyRange(ligne, 9).Value = cparr4
    tbl.DataBodyRange(ligne, 10).Value = cparr5
    tbl.DataBodyRange(ligne, 11).Value = cvfrecevabilite
    tbl.DataBodyRange(ligne, 12).Value = cmodif
    tbl.DataBodyRange(ligne, 13).Value = cnouveau

End Function

Public Function insertToEtatActuel(tbl As ListObject)

    Dim ligne As Integer

    tbl.ListRows.Add AlwaysInsert:=True
    
    ligne = tbl.ListRows.count

    tbl.DataBodyRange(ligne, 1).Value = ccodeGED
    tbl.DataBodyRange(ligne, 2).Value = ctitre
    tbl.DataBodyRange(ligne, 3).Value = cetatDoc
    tbl.DataBodyRange(ligne, 4).Value = cstatutBordereau
    tbl.DataBodyRange(ligne, 5).Value = cpremierRegard
    tbl.DataBodyRange(ligne, 6).Value = cparr1
    tbl.DataBodyRange(ligne, 7).Value = cparr2
    tbl.DataBodyRange(ligne, 8).Value = cparr3
    tbl.DataBodyRange(ligne, 9).Value = cparr4
    tbl.DataBodyRange(ligne, 10).Value = cparr5
    tbl.DataBodyRange(ligne, 11).Value = cvfrecevabilite
    tbl.DataBodyRange(ligne, 12).Value = cmodif
    tbl.DataBodyRange(ligne, 13).Value = cnouveau

End Function



