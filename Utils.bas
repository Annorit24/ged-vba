Attribute VB_Name = "Utils"
Public Const PG_SHEET_INDEX As Integer = 3
Public Const PE_SHEET_INDEX As Integer = 4
Public Const COMP_SHEET_INDEX As Integer = 5

Public Const CODE_GED_INDEX = 18
Public Const TITRE_INDEX = 19
Public Const ETAT_DOC_INDEX = 14
Public Const STATUT_BORDEREAU_INDEX = 25
Public Const PREMIER_REGARD_INDEX = 31
Public Const PARR1_INDEX = 40
Public Const PARR2_INDEX = 49
Public Const PARR3_INDEX = 58
Public Const PARR4_INDEX = 67
Public Const PARR5_INDEX = 76
Public Const VF_RECEVABILITE_INDEX = 121

'Pour tableau ETAT_ACTUEL
Public Function convertirLigneEnDonneesFicheA(ligne As Integer, tbl As ListObject) As donnesFiche
    
    Dim donnesFiche As donnesFiche
    Set donnesFiche = New donnesFiche
    
    donnesFiche.codeGED = tbl.DataBodyRange(ligne, 1).Value
    donnesFiche.TITRE = tbl.DataBodyRange(ligne, 2).Value
    donnesFiche.etatDoc = tbl.DataBodyRange(ligne, 3).Value
    donnesFiche.statutBordereau = tbl.DataBodyRange(ligne, 4).Value
    donnesFiche.premierRegard = tbl.DataBodyRange(ligne, 5).Value
    donnesFiche.parr1 = tbl.DataBodyRange(ligne, 6).Value
    donnesFiche.parr2 = tbl.DataBodyRange(ligne, 7).Value
    donnesFiche.parr3 = tbl.DataBodyRange(ligne, 8).Value
    donnesFiche.parr4 = tbl.DataBodyRange(ligne, 9).Value
    donnesFiche.parr5 = tbl.DataBodyRange(ligne, 10).Value
    donnesFiche.vfrecevabilite = tbl.DataBodyRange(ligne, 11).Value
    donnesFiche.modif = tbl.DataBodyRange(ligne, 12).Value
    donnesFiche.nouveau = tbl.DataBodyRange(ligne, 13).Value

    Set convertirLigneEnDonneesFicheA = donnesFiche


End Function

'Pour tableau RAPPORT_AVANCEMENT_XX
Public Function convertirLigneEnDonneesFicheB(ligne As Integer, tbl As ListObject) As donnesFiche

    Dim donnesFiche As donnesFiche
    Set donnesFiche = New donnesFiche
    
    donnesFiche.codeGED = tbl.DataBodyRange(ligne, CODE_GED_INDEX).Value
    donnesFiche.TITRE = tbl.DataBodyRange(ligne, TITRE_INDEX).Value
    donnesFiche.etatDoc = tbl.DataBodyRange(ligne, ETAT_DOC_INDEX).Value
    donnesFiche.statutBordereau = tbl.DataBodyRange(ligne, STATUT_BORDEREAU_INDEX).Value
    donnesFiche.premierRegard = tbl.DataBodyRange(ligne, PREMIER_REGARD_INDEX).Value
    donnesFiche.parr1 = tbl.DataBodyRange(ligne, PARR1_INDEX).Value
    donnesFiche.parr2 = tbl.DataBodyRange(ligne, PARR2_INDEX).Value
    donnesFiche.parr3 = tbl.DataBodyRange(ligne, PARR3_INDEX).Value
    donnesFiche.parr4 = tbl.DataBodyRange(ligne, PARR4_INDEX).Value
    donnesFiche.parr5 = tbl.DataBodyRange(ligne, PARR5_INDEX).Value
    donnesFiche.vfrecevabilite = tbl.DataBodyRange(ligne, VF_RECEVABILITE_INDEX).Value

    Set convertirLigneEnDonneesFicheB = donnesFiche
    
End Function

Sub clearDebugConsole()
    For i = 0 To 100
        Debug.Print ""
    Next i
End Sub

Sub WaitFor(NumOfSeconds As Single)
    Dim SngSec As Single
    SngSec = Timer + NumOfSeconds

    Do While Timer < SngSec
        DoEvents
   Loop
End Sub
