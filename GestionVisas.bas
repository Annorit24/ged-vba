Attribute VB_Name = "GestionVisas"
Public Const selecIndexCol As Integer = 75

Sub test()

    Dim res As ArrayList
    Set res = recupListeSelec
    
    Dim i As Integer
    
    For i = 0 To res.count - 1
        DL_VISA_FORM.Liste.AddItem res(i)
    Next i

    DL_VISA_FORM.Show
    
End Sub

Public Sub lancerTelechargement()

    Dim cmd As String
    cmd = buildJavaCommand
    
    Debug.Print cmd
    
    Call lancerAppJava(cmd)

End Sub


Function buildJavaCommand() As String

    Dim javaPath As String
    Dim appPath As String
    Dim javaCommand As String
    Dim res As ArrayList
    
    Dim username As String
    Dim pwd As String

    username = ThisWorkbook.Sheets(1).Cells(4, 3)
    pwd = ThisWorkbook.Sheets(1).Cells(5, 3)

    javaPath = Replace(SharePointUtils.GetLocalPath(ThisWorkbook.FullName), ThisWorkbook.Name, "") & "\x-ressources\jdk\bin\javaw"
    appPath = """" & Replace(SharePointUtils.GetLocalPath(ThisWorkbook.FullName), ThisWorkbook.Name, "") & "\x-ressources\app-all.jar"" "

    javaCommand = javaPath & " -jar " & appPath & username & " " & pwd & " visa"
    
    Dim i As Integer
    For i = 0 To DL_VISA_FORM.Liste.ListCount - 1
        javaCommand = javaCommand & " " & Split(DL_VISA_FORM.Liste.List(i), "#")(0)
    Next i
    
    buildJavaCommand = javaCommand
    
End Function

Public Function lancerAppJava(cmd As String)
    Dim winShell As Object: Set winShell = CreateObject("WScript.Shell")
    Dim WaitOnReturn As Boolean: WaitOnReturn = False
    Dim windowStyle As Integer: windowStyle = 1

    Call winShell.Exec(cmd)
    Set winShell = Nothing

End Function

Function recupListeSelec() As ArrayList

    Dim res As New ArrayList
    Dim tableauCOMP As ListObject
    Set tableauCOMP = ThisWorkbook.Sheets(Utils.COMP_SHEET_INDEX).ListObjects(1)
    
    Debug.Print tableauCOMP.Range.Rows.count
    
    For i = 1 To tableauCOMP.Range.Rows.count
        
        Dim val As String
        val = tableauCOMP.DataBodyRange(i, selecIndexCol).Value
    
        If val = "x" Then
            Dim allTitre As String
            allTitre = tableauCOMP.DataBodyRange(i, 10).Value
            allTitre = allTitre & "#" & tableauCOMP.DataBodyRange(i, 11).Value
            
            res.Add allTitre
        End If
        
    
    Next i
    
    Set recupListeSelec = res

End Function
