Attribute VB_Name = "GestionRetard"
Public Function ajouterRetards(i As Integer)


    Dim table As ListObject
    Set table = ThisWorkbook.Sheets(i).ListObjects(1)
    
    ThisWorkbook.Sheets(i).Activate
    Call setRetard3(table)
    Call setRetard2(table)
    Call setRetard1(table)
    
    Columns("D:F").ColumnWidth = "42"
    Rows("2:2").RowHeight = 30
    
End Function

Public Function setRetard1(table As ListObject)
    table.ListColumns.Add Position:=4
    Dim lc As ListColumn
    Set lc = table.ListColumns(4)
    lc.Range.NumberFormat = "General"
    lc.Name = "Envoi document jours de retards" & Chr(13) & Chr(10) & " ( Documents pas encors difus�s)"
    lc.DataBodyRange.FormulaLocal = "=SI(ET([@[Date pr�visionnelle d�envoi en validation]] <= AUJOURDHUI(); [@[Date pr�visionnelle d�envoi en validation]]<> """" ); [@[Date pr�visionnelle d�envoi en validation]] - AUJOURDHUI(); """" )"
    lc.Range.FormatConditions.Add Type:=xlExpression, Formula1:="=D3 = """""
    Call setFormatCondition(lc)
End Function

Public Function setRetard2(table As ListObject)
    table.ListColumns.Add Position:=4
    Dim lc As ListColumn
    Set lc = table.ListColumns(4)
    lc.Range.NumberFormat = "General"
    lc.Name = "Validation document jours de retards " & Chr(13) & Chr(10) & " ( Documents pas encore vis�s )"
    lc.DataBodyRange.FormulaLocal = "=SI(ET([@[Date d''envoi en validation]]+21-AUJOURDHUI() < 0; NON([@[Date d''envoi en validation]] = """"); [@[Etat du document]]=""en validation""); ([@[Date d''envoi en validation]]+21-AUJOURDHUI()) * (-1);"""")"
    Call setFormatCondition(lc)
End Function

Public Function setRetard3(table As ListObject)
    table.ListColumns.Add Position:=4
    Dim lc As ListColumn
    Set lc = table.ListColumns(4)
    lc.Range.NumberFormat = "General"
    lc.Name = "Validation document jours de retards " & Chr(13) & Chr(10) & "  ( Documents vis�s )"
    lc.DataBodyRange.FormulaLocal = "=SI(ET([@[Etat du document]] <> ""en cr�ation""; [@[Etat du document]] <> ""en validation"";[@[Date r�elle de recevabilit�          ]]<> """");  SI([@[Date d''envoi en validation]] + 21 -[@[Date r�elle de recevabilit�          ]] < 0; """";  [@[Date d''envoi en validation]] + 21 - [@[Date r�elle de recevabilit�          ]]); """")"
    Call setFormatCondition(lc)
End Function

Private Function setFormatCondition(lc As ListColumn)

    lc.Range.FormatConditions.Add Type:=xlExpression, Formula1:="=D2 = """" "
    lc.Range.FormatConditions(lc.Range.FormatConditions.count).SetFirstPriority
    With lc.Range.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .ThemeColor = xlThemeColorAccent6
        .TintAndShade = 0
    End With
    lc.Range.FormatConditions(1).StopIfTrue = False

    lc.Range.FormatConditions.Add Type:=xlExpression, Formula1:="=D2 <> """" "
    lc.Range.FormatConditions(lc.Range.FormatConditions.count).SetFirstPriority
    With lc.Range.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .Color = 192
        .TintAndShade = 0
    End With
    lc.Range.FormatConditions(1).StopIfTrue = False

End Function

